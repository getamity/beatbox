"""beatbox: Makes the salesforce.com SOAP API easily accessible."""

__version__ = '32.15'
__author__ = "Simon Fell et al"
__credits__ = "Mad shouts to the sforce possie"
__copyright__ = "(C) 2006 Simon Fell. GNU GPL 2."

import httplib

import httplib2
import logging
import socket
from urlparse import urlparse
from StringIO import StringIO
import gzip
import datetime
import xmltramp
from xmltramp import islst
from xml.sax.saxutils import XMLGenerator
from xml.sax.xmlreader import AttributesNSImpl

# global constants for namespace strings, used during serialization
_partnerNs = "urn:partner.soap.sforce.com"
_sobjectNs = "urn:sobject.partner.soap.sforce.com"
_envNs = "http://schemas.xmlsoap.org/soap/envelope/"
_noAttrs = AttributesNSImpl({}, {})

DEFAULT_SERVER_URL = 'https://login.salesforce.com/services/Soap/u/32.0'

# global constants for xmltramp namespaces, used to access response data
_tPartnerNS = xmltramp.Namespace(_partnerNs)
_tSObjectNS = xmltramp.Namespace(_sobjectNs)
_tSoapNS = xmltramp.Namespace(_envNs)

# global config
gzipRequest = True    # are we going to gzip the request ?
gzipResponse = True   # are we going to tell teh server to gzip the response ?
forceHttp = False     # force all connections to be HTTP, for debugging

logger = logging.getLogger('beatbox')


class SOAPConnection:
    def __init__(self, serverURL, platformCertsPath=None, connection_timeout=None):
        self.serverUrl = serverURL
        # we'll extract and sanitize the pieces of the URL
        (self.serverScheme, self.serverHost, self.serverPath, params, query, frag) = urlparse(self.serverUrl)
        self.serverScheme = self.serverScheme.upper()  #normalize to upper case
        self.platformCertsPath = platformCertsPath
        self.HTTPConnection = None
        self.SOAPSessionId = ""
        self.connection_timeout = connection_timeout

    # does all the grunt work,
    #   serializes the request,
    #   makes a http request,
    #   passes the response to tramp
    #   checks for soap fault
    #   todo: check for mU='1' headers
    #   returns a dict containing the following:
    #   'body': the relevant result from the body child
    #   'header': the result header (for diagnostics info)
    #   'request_count': the number of times a remote request was made
    def postSOAP(self, soapEnvelope, alwaysReturnList=False):
        logger.debug(self.__class__)
        headers = {
            "User-Agent": "BeatBox/" + __version__,
            "SOAPAction": "\"\"",
            "Content-Type": "text/xml; charset=utf-8",
        }
        if gzipResponse:
            headers['accept-encoding'] = 'gzip'
        if gzipRequest:
            headers['content-encoding'] = 'gzip'

        max_attempts = 3
        response = None
        attempt = 1
        while not response and attempt <= max_attempts:
            try:
                self.ensureConnection()
                self.HTTPConnection.request("POST", self.serverPath, soapEnvelope.makeEnvelope(self.SOAPSessionId), headers)
                response = self.HTTPConnection.getresponse()
                rawResponse = response.read()
            except (httplib2.HttpLib2Error, socket.error, httplib.HTTPException):
                # added httplib exceptions to retry logic
                response = None
                self.closeConnection()
                attempt += 1
        if not response:
            raise RuntimeError('No response from Salesforce')

        result = dict()
        result["request_count"] = attempt

        if response.getheader('content-encoding', '') == 'gzip':
            rawResponse = gzip.GzipFile(fileobj=StringIO(rawResponse)).read()

        tramp = xmltramp.parse(rawResponse)
        try:
            faultString = str(tramp[_tSoapNS.Body][_tSoapNS.Fault].faultstring)
            faultCode = str(tramp[_tSoapNS.Body][_tSoapNS.Fault].faultcode).split(':')[-1]
            if faultCode == 'INVALID_SESSION_ID':
                raise SessionTimeoutError(faultCode, faultString)
            else:
                raise SoapFaultError(faultCode, faultString)
        except KeyError:
            pass
        # first child of body is XXXXResponse
        result_body = tramp[_tSoapNS.Body][0]
        # it contains either a single child, or for a batch call multiple children

        # now extract the header info
        try:
            result["header"] = tramp[_tSoapNS.Header]
        except KeyError:
            result["header"] = None
        if alwaysReturnList or len(result_body) > 1:
            result["body"] = result_body[:]
        else:
            result["body"] = result_body[0]

        return result

    def ensureConnection(self, serverUrl=None, sessionId=None):
        if serverUrl:
            # check if current connection matches new serverURL.
            (scheme, host, path, params, query, frag) = urlparse(serverUrl)
            # should I log if there is change in scheme type (HTTP or HTTPS)?
            if scheme != self.serverScheme or host != self.serverHost:
                self.closeConnection()
                self.serverScheme = scheme
                self.serverHost = host
            self.serverPath = path
            self.serverUrl = serverUrl

        if sessionId is not None:  # only update session ID if it was specified
            self.SOAPSessionId = sessionId

        if self.HTTPConnection is None:
            # httplib does not properly validate SSL certificates and does not check that they match
            # the hostname... Fortunately, httplib2 does it properly, and has drop-in replacements for httplib
            if forceHttp or self.serverScheme == 'HTTP':
                self.HTTPConnection = httplib2.HTTPConnectionWithTimeout(self.serverHost,
                                                                         timeout=self.connection_timeout)
            else:
                self.HTTPConnection = httplib2.HTTPSConnectionWithTimeout(self.serverHost,
                                                                          ca_certs=self.platformCertsPath,
                                                                          timeout=self.connection_timeout)

    def closeConnection(self):
        if self.HTTPConnection is not None:
            self.HTTPConnection.close()
            self.HTTPConnection = None

    def isConnected(self):
        """ First pass at a method to check if we're connected or not """
        if self.HTTPConnection and self.HTTPConnection._HTTPConnection__state == 'Idle':
            return True
        return False


# the main sforce client proxy class
class Client:
    def __init__(self, serverUrl=None, platformCertsPath=None, connection_timeout=None):
        self.batchSize = 500
        self.serverUrl = serverUrl or DEFAULT_SERVER_URL  # main server/login url
        self.commandUrl = None
        self.__conn = SOAPConnection(self.serverUrl, platformCertsPath, connection_timeout=connection_timeout)
        self.requestCount = 0
        self.sf_api_counts = dict()  # "min", "max", "limit"
        self.sf_api_app_counts = dict()  # "min", "max", "limit"

    def __del__(self):
        if self.__conn is not None:
            self.__conn.closeConnection()

    @staticmethod
    def _get_api_limit_data_by_type(header_elem, type_string):
        value = 0
        limit = 0
        try:
            limit_info_header = header_elem[_tPartnerNS.LimitInfoHeader]  # get <LimitInfoHeader>
            limit_info_list = limit_info_header[_tPartnerNS.limitInfo:]  # get all <limitInfo> child elements

            for limit_info in limit_info_list:
                try:
                    if str(limit_info[_tPartnerNS.type]) == type_string:  # compare <type>
                        value = int(str(limit_info[_tPartnerNS.current]))  # get <current> and make it an int
                        limit = int(str(limit_info[_tPartnerNS.limit]))  # get <limit> and make it an int
                        break
                except KeyError:
                    pass  # keep trying
        except KeyError:  # if any of the element names above don't exist, it comes here
            value = 0
            limit = 0
        return value, limit

    # takes a post result header and updates api limit stats
    # it is assumed that there is only 0 or 1 of each type of limitInfo items
    def _update_api_limit_stats(self, soap_header_elem):
        value, limit = self._get_api_limit_data_by_type(soap_header_elem, "API REQUESTS")
        # only process if limit is non-zero
        if limit:
            self.sf_api_counts["limit"] = limit
            if not self.sf_api_counts.get("min"):
                self.sf_api_counts["min"] = value
            if value > self.sf_api_counts.get("max", 0):
                self.sf_api_counts["max"] = value

        value, limit = self._get_api_limit_data_by_type(soap_header_elem, "API REQUESTS PER APP")
        if limit:
            self.sf_api_app_counts["limit"] = limit
            if not self.sf_api_app_counts.get("min"):
                self.sf_api_app_counts["min"] = value
            if value > self.sf_api_app_counts.get("max", 0):
                self.sf_api_app_counts["max"] = value

    # takes a post result and updates request count and
    # returns the result 'body'
    def _processPostResult(self, post_result):
        self.requestCount += post_result["request_count"]
        if post_result.get("header", None) is not None:
            self._update_api_limit_stats(post_result.get("header"))
        return post_result["body"]

    # login, the serverUrl and sessionId are automatically handled,
    # returns the loginResult structure
    def login(self, username, password):
        self.useSession(None, self.serverUrl)  # kill the old session, reset URL to the login url
        lr = self.__conn.postSOAP(LoginRequest( username, password))
        # Note: login request does not return a soap header, just a body
        request_body = self._processPostResult(lr)
        self.useSession(str(request_body[_tPartnerNS.sessionId]), str(request_body[_tPartnerNS.serverUrl]))
        return request_body

    # initialize from an existing sessionId & serverUrl, useful if we're being launched
    # via a custom link
    def useSession(self, sessionId, sessionUrl):
        # reset local storage for sessionId and commandURL
        self.commandUrl = None

        self.__conn.ensureConnection(serverUrl=sessionUrl, sessionId=sessionId)
        self.commandUrl = sessionUrl

    # set the batchSize property on the Client instance to change the batchsize for query/queryMore
    def query(self, soql):
        pr = self.__conn.postSOAP(QueryRequest(self.batchSize, soql))
        return self._processPostResult(pr)

    def queryMore(self, queryLocator):
        pr = self.__conn.postSOAP(QueryMoreRequest(self.batchSize, queryLocator))
        return self._processPostResult(pr)

    def search(self, sosl):
        pr = self.__conn.postSOAP(SearchRequest(self.batchSize, sosl))
        return self._processPostResult(pr)

    def getUpdated(self, sObjectType, start, end):
        pr = self.__conn.postSOAP(GetUpdatedRequest(sObjectType, start, end))
        return self._processPostResult(pr)

    def getDeleted(self, sObjectType, start, end):
        pr = self.__conn.postSOAP(GetDeletedRequest(sObjectType, start, end))
        return self._processPostResult(pr)

    def retrieve(self, fields, sObjectType, ids):
        pr =  self.__conn.postSOAP(RetrieveRequest(fields, sObjectType, ids))
        return self._processPostResult(pr)

    # sObjects can be 1 or a list, returns a single save result or a list
    def create(self, sObjects):
        pr = self.__conn.postSOAP(CreateRequest(sObjects))
        return self._processPostResult(pr)

    # sObjects can be 1 or a list, returns a single save result or a list
    def update(self, sObjects):
        pr = self.__conn.postSOAP(UpdateRequest(sObjects))
        return self._processPostResult(pr)

    # sObjects can be 1 or a list, returns a single upsert result or a list
    def upsert(self, externalIdName, sObjects):
        pr = self.__conn.postSOAP(UpsertRequest(externalIdName, sObjects))
        return self._processPostResult(pr)

    # ids can be 1 or a list, returns a single delete result or a list
    def delete(self, ids):
        pr = self.__conn.postSOAP(DeleteRequest(ids))
        return self._processPostResult(pr)

    # sObjectTypes can be 1 or a list, returns a single describe result or a list of them
    def describeSObjects(self, sObjectTypes):
        pr = self.__conn.postSOAP(DescribeSObjectsRequest(sObjectTypes))
        return self._processPostResult(pr)

    def describeGlobal(self):
        pr = self.__conn.postSOAP(AuthenticatedRequest("describeGlobal"))
        return self._processPostResult(pr)

    def describeLayout(self, sObjectType):
        pr = self.__conn.postSOAP(DescribeLayoutRequest(sObjectType))
        return self._processPostResult(pr)

    def describeTabs(self):
        pr = self.__conn.postSOAP(AuthenticatedRequest("describeTabs"), alwaysReturnList=True)
        return self._processPostResult(pr)

    def getServerTimestamp(self):
        pr = self.__conn.postSOAP(AuthenticatedRequest("getServerTimestamp"))
        body = self._processPostResult(pr)
        return str(body[_tPartnerNS.timestamp])

    def resetPassword(self, userId):
        pr = self.__conn.postSOAP(ResetPasswordRequest(userId))
        return self._processPostResult(pr)

    def setPassword(self, userId, password):
        pr = self.__conn.postSOAP(SetPasswordRequest(userId, password))
        self._processPostResult(pr) # update request count

    def getUserInfo(self):
        pr = self.__conn.postSOAP(AuthenticatedRequest("getUserInfo"))
        return self._processPostResult(pr)

    def convertLead(self, leadConverts):
        pr = self.__conn.postSOAP(ConvertLeadRequest(leadConverts))
        return self._processPostResult(pr)


# general purpose xml writer, does a bunch of useful stuff above & beyond XmlGenerator
class XmlWriter:
    def __init__(self, doGzip):
        self.__buf = StringIO("")
        if doGzip:
            self.__gzip = gzip.GzipFile(mode='wb', fileobj=self.__buf)
            stm = self.__gzip
        else:
            stm = self.__buf
            self.__gzip = None
        self.xg = XMLGenerator(stm, "utf-8")
        self.xg.startDocument()
        self.__elems = []

    def startPrefixMapping(self, prefix, namespace):
        self.xg.startPrefixMapping(prefix, namespace)

    def endPrefixMapping(self, prefix):
        self.xg.endPrefixMapping(prefix)

    def startElement(self, namespace, name, attrs=_noAttrs):
        self.xg.startElementNS((namespace, name), name, attrs)
        self.__elems.append((namespace, name))

    # if value is a list, then it writes out repeating elements, one for each value
    def writeStringElement(self, namespace, name, value, attrs=_noAttrs):
        if islst(value):
            for v in value:
                self.writeStringElement(namespace, name, v, attrs)
        elif isinstance(value, dict):
            self.startElement(namespace, name, attrs)
            # Type must always come first, even in embedded objects.
            type_entry = value['type']
            self.writeStringElement(namespace, 'type', type_entry, attrs)
            del value['type']
            for k, v in value.items():
                self.writeElement(namespace, k, v, attrs)
            self.endElement()
        else:
            self.startElement(namespace, name, attrs)
            self.characters(value)
            self.endElement()

    def endElement(self):
        e = self.__elems[-1]
        self.xg.endElementNS(e, e[1])
        del self.__elems[-1]

    def characters(self, s):
        # todo base64 ?
        if isinstance(s, datetime.datetime):
            # todo, timezones
            s = s.isoformat()
        elif isinstance(s, datetime.date):
            # todo, try isoformat
            s = "%04d-%02d-%02d" % (s.year, s.month, s.day)
        elif isinstance(s, (int, float, long)):
            s = str(s)
        self.xg.characters(s)

    def endDocument(self):
        self.xg.endDocument()
        if (self.__gzip is not None):
            self.__gzip.close()
        return self.__buf.getvalue()


# exception class for soap faults
class SoapFaultError(Exception):
    def __init__(self, faultCode, faultString):
        self.faultCode = faultCode
        self.faultString = faultString

    def __str__(self):
        return repr(self.faultCode) + " " + repr(self.faultString)


class SessionTimeoutError(Exception):
    """SessionTimeouts are recoverable errors, merely needing the creation
       of a new connection, we create a new exception type, so these can
       be identified and handled seperately from SoapFaultErrors
    """
    def __init__(self, faultCode, faultString):
        self.faultCode = faultCode
        self.faultString = faultString

    def __str__(self):
        return repr(self.faultCode) + " " + repr(self.faultString)


# soap specific stuff ontop of XmlWriter
class SoapWriter(XmlWriter):
    def __init__(self):
        XmlWriter.__init__(self, gzipRequest)
        self.startPrefixMapping("s", _envNs)
        self.startPrefixMapping("p", _partnerNs)
        self.startPrefixMapping("o", _sobjectNs)
        self.startElement(_envNs, "Envelope")

    def endDocument(self):
        self.endElement()  # envelope
        self.endPrefixMapping("o")
        self.endPrefixMapping("p")
        self.endPrefixMapping("s")
        return XmlWriter.endDocument(self)


# processing for a single soap request / response
class SoapEnvelope:
    def __init__(self, operationName, clientId="BeatBox/" + __version__):
        self.operationName = operationName
        self.clientId = clientId

    def writeHeaders(self, writer, sessionID):
        pass

    def writeBody(self, writer):
        pass

    def makeEnvelope(self, sessionId=None):
        s = SoapWriter()
        s.startElement(_envNs, "Header")
        s.characters("\n")
        s.startElement(_partnerNs, "CallOptions")
        s.writeStringElement(_partnerNs, "client", self.clientId)
        s.endElement()
        s.characters("\n")
        self.writeHeaders(s, sessionId or "")  # writeHeaders() expects a non-None session Id string
        s.endElement()  # Header
        s.startElement(_envNs, "Body")
        s.characters("\n")
        s.startElement(_partnerNs, self.operationName)
        self.writeBody(s)
        s.endElement()  # operation
        s.endElement()  # body
        return s.endDocument()


class LoginRequest(SoapEnvelope):
    def __init__(self, username, password):
        SoapEnvelope.__init__(self, "login")
        self.__username = username
        self.__password = password

    def writeBody(self, s):
        s.writeStringElement(_partnerNs, "username", self.__username)
        s.writeStringElement(_partnerNs, "password", self.__password)


# base class for all methods that require a sessionId
class AuthenticatedRequest(SoapEnvelope):
    def __init__(self, operationName):
        SoapEnvelope.__init__(self, operationName)

    def writeHeaders(self, s, sessionId):
        s.startElement(_partnerNs, "SessionHeader")
        s.writeStringElement(_partnerNs, "sessionId", sessionId)
        s.endElement()

    def writeSObjects(self, s, sObjects, elemName="sObjects"):
        if islst(sObjects):
            for o in sObjects:
                self.writeSObjects(s, o, elemName)
        else:
            s.startElement(_partnerNs, elemName)
            # type has to go first
            s.writeStringElement(_sobjectNs, "type", sObjects['type'])
            for fn in sObjects.keys():
                if (fn != 'type'):
                    s.writeStringElement(_sobjectNs, fn, sObjects[fn])
            s.endElement()


class QueryOptionsRequest(AuthenticatedRequest):
    def __init__(self, batchSize, operationName):
        AuthenticatedRequest.__init__(self, operationName)
        self.batchSize = batchSize

    def writeHeaders(self, s, sessionId):
        AuthenticatedRequest.writeHeaders(self, s, sessionId)
        s.startElement(_partnerNs, "QueryOptions")
        s.writeStringElement(_partnerNs, "batchSize", self.batchSize)
        s.endElement()


class QueryRequest(QueryOptionsRequest):
    def __init__(self, batchSize, soql):
        QueryOptionsRequest.__init__(self, batchSize, "query")
        self.__query = soql

    def writeBody(self, s):
        s.writeStringElement(_partnerNs, "queryString", self.__query)


class QueryMoreRequest(QueryOptionsRequest):
    def __init__(self, batchSize, queryLocator):
        QueryOptionsRequest.__init__(self, batchSize, "queryMore")
        self.__queryLocator = queryLocator

    def writeBody(self, s):
        s.writeStringElement(_partnerNs, "queryLocator", self.__queryLocator)


class SearchRequest(QueryOptionsRequest):
    def __init__(self, batchSize, sosl):
        QueryOptionsRequest.__init__(self, batchSize, "search")
        self.__search = sosl

    def writeBody(self, s):
        s.writeStringElement(_partnerNs, "searchString", self.__search)


class GetUpdatedRequest(AuthenticatedRequest):
    def __init__(self, sObjectType, start, end, operationName="getUpdated"):
        AuthenticatedRequest.__init__(self, operationName)
        self.__sObjectType = sObjectType
        self.__start = start
        self.__end = end

    def writeBody(self, s):
        s.writeStringElement(_partnerNs, "sObjectType", self.__sObjectType)
        s.writeStringElement(_partnerNs, "startDate", self.__start)
        s.writeStringElement(_partnerNs, "endDate", self.__end)


class GetDeletedRequest(GetUpdatedRequest):
    def __init__(self, sObjectType, start, end):
        GetUpdatedRequest.__init__(self, sObjectType, start, end, "getDeleted")


class UpsertRequest(AuthenticatedRequest):
    def __init__(self, externalIdName, sObjects):
        AuthenticatedRequest.__init__(self, "upsert")
        self.__externalIdName = externalIdName
        self.__sObjects = sObjects

    def writeBody(self, s):
        s.writeStringElement(_partnerNs, "externalIDFieldName", self.__externalIdName)
        self.writeSObjects(s, self.__sObjects)


class UpdateRequest(AuthenticatedRequest):
    def __init__(self, sObjects, operationName="update"):
        AuthenticatedRequest.__init__(self, operationName)
        self.__sObjects = sObjects

    def writeBody(self, s):
        self.writeSObjects(s, self.__sObjects)


class CreateRequest(UpdateRequest):
    def __init__(self, sObjects):
        UpdateRequest.__init__(self, sObjects, "create")


class DeleteRequest(AuthenticatedRequest):
    def __init__(self, ids):
        AuthenticatedRequest.__init__(self, "delete")
        self.__ids = ids

    def writeBody(self, s):
        s.writeStringElement(_partnerNs, "id", self.__ids)


class RetrieveRequest(AuthenticatedRequest):
    def __init__(self, fields, sObjectType, ids):
        AuthenticatedRequest.__init__(self, "retrieve")
        self.__fields = fields
        self.__sObjectType = sObjectType
        self.__ids = ids

    def writeBody(self, s):
        s.writeStringElement(_partnerNs, "fieldList", self.__fields)
        s.writeStringElement(_partnerNs, "sObjectType", self.__sObjectType)
        s.writeStringElement(_partnerNs, "ids", self.__ids)


class ResetPasswordRequest(AuthenticatedRequest):
    def __init__(self, userId):
        AuthenticatedRequest.__init__(self, "resetPassword")
        self.__userId = userId

    def writeBody(self, s):
        s.writeStringElement(_partnerNs, "userId", self.__userId)


class SetPasswordRequest(AuthenticatedRequest):
    def __init__(self, userId, password):
        AuthenticatedRequest.__init__(self, "setPassword")
        self.__userId = userId
        self.__password = password

    def writeBody(self, s):
        s.writeStringElement(_partnerNs, "userId", self.__userId)
        s.writeStringElement(_partnerNs, "password", self.__password)


class DescribeSObjectsRequest(AuthenticatedRequest):
    def __init__(self, sObjectTypes):
        AuthenticatedRequest.__init__(self, "describeSObjects")
        self.__sObjectTypes = sObjectTypes

    def writeBody(self, s):
        s.writeStringElement(_partnerNs, "sObjectType", self.__sObjectTypes)


class DescribeLayoutRequest(AuthenticatedRequest):
    def __init__(self, sObjectType):
        AuthenticatedRequest.__init__(self, "describeLayout")
        self.__sObjectType = sObjectType

    def writeBody(self, s):
        s.writeStringElement(_partnerNs, "sObjectType", self.__sObjectType)


class ConvertLeadRequest(AuthenticatedRequest):
    """
    Converts a Lead to an Account. See also:
    http://www.salesforce.com/us/developer/docs/api/Content/sforce_api_calls_convertlead.htm
    """
    def __init__(self, leadConverts):
        AuthenticatedRequest.__init__(self, "convertLead")
        if not isinstance(leadConverts, list):
            leadConverts = [leadConverts]
        self.__leadConverts = leadConverts

    def writeBody(self, s):
        for leadConvert in self.__leadConverts:
            s.startElement(None, 'leadConverts')
            for f in ('accountId', 'contactId', 'convertedStatus', 'doNotCreateOpportunity',
                      'leadId', 'opportunityName', 'overwriteLeadSource', 'ownerId',
                      'sendNotificationEmail'):
                if f in leadConvert:
                    s.writeStringElement(_partnerNs, f, leadConvert[f])
            s.endElement()
